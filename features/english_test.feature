Feature: GL Test

 Scenario: English test result
   
   Given ENG I am on the GL test environment website
   And ENG I select to take the English test
   And ENG I select start test button
   And ENG I choose next button after reading the instruction
   When ENG I answer the first 2 set of questions and choose the next button
   And ENG I answer another 2 set of questions and select the next button again
   And ENG I select the answers for the next 3 set of questions
   And ENG I select the submit button to end the test
   Then ENG I will see "<output>" displayed on the sreen
