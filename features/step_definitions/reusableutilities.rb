require 'rubygems'
require 'cucumber'
require "watir"
require 'page-object'
require 'test/unit/assertions'
include Test::Unit::Assertions
# require "active_support/all"


class ReusableUtilities
  include PageObject

def open
case ENV['execution']
when 'development'
  $browser.goto $DEV_ACCEPTANCE_TEST_URL
  $browser.driver.manage.window.maximize
when 'test'
  $browser.goto $GL_ACCEPTANCE_TEST_URL
  $browser.driver.manage.window.maximize
when 'staged'
  $browser.goto $STG_ACCEPTANCE_TEST_URL
  $browser.driver.manage.window.maximize
when 'pipeline'
  $browser.goto $PIPELINE_ACCEPTANCE_TEST_URL
  $browser.driver.manage.window.maximize
when 'local'
  $browser.goto $CLOUD_ACCEPTANCE_TEST_URL
  $browser.driver.manage.window.maximize
when 'production'
  $browser.goto $PROD_ACCEPTANCE_TEST_URL
  $browser.driver.manage.window.maximize
end
end

def screenshot_capture(screen_name)
  $browser.screenshot.save ("output/screenshots/#{screen_name}.png")
end

def page_element_checks(element_id)
# check if id is avalailable on a screen
  assert_equal true, $browser.element(id: element_id).exists?, 'element ID does not exist on the page'
end

def regular_page_checks(screen_name, title)
  #check the page heading
  #assert_equal true, $browser.element(id: 'question-heading').exist?, screen_name + ' Screen: Heading text does not exist'
  #check the page title
  assert_equal title, $browser.title, screen_name + ' Screen: Page title incorrect'
end

def result_page_checks(screen_name, title)
  #check the page heading
  #assert_equal true, $browser.element(id: 'result-heading').exist?, screen_name + ' Screen: Heading title incorrect'
  #check the page title
  assert_equal title, $browser.title, screen_name + ' Screen: Page title incorrect'
end

def element_check(element_locator, screen_name)
  #checks the back link exists
  assert_equal true, $browser.element(element: element_locator).exists?, + screen_name + ' This element does not exist on the screen'

end

def url_check(screen_name)
  #check for feedback link
  assert_equal true, $browser.link(href: 'https://testwise.testingforschools.com/tests/player-demo-iseb/#/').exist?, screen_name + ' Header: Feedback link does not exist or is not shown'
end

def footer_check(screen_name)
  #check for footer existing
  assert_equal true, $browser.element(id: 'footer').exist?, screen_name + ' Footer: Footer does not exist or is not shown'

  #check for privacy link
  assert_equal true, $browser.link(href: "https://www.nhsbsa.nhs.uk/our-policies/privacy/check-if-you-get-free-nhs-prescriptions-privacy-notice").exist?, screen_name + ' Footer: Privacy link does not exist or is not shown'

  #check for cookies link
  assert_equal true, $browser.link(href: "https://www.nhsbsa.nhs.uk/our-policies/cookies").exist?, screen_name + ' Footer: Cookies link does not exist or is not shown'

  #check for T&C link
  assert_equal true, $browser.link(href: "https://www.nhsbsa.nhs.uk/our-policies/terms-and-conditions").exist?, screen_name + ' Footer: T&C link does not exist or is not shown'

  #check for BSA link
  assert_equal true, $browser.link(href: "http://www.nhsbsa.nhs.uk").exist?, screen_name + ' Footer: BSA link does not exist or is not shown'

  #check for open government link
  assert_equal true, $browser.link(href: "https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/").exist?, screen_name + ' Footer: Open government link does not exist or is not shown'

end
end
