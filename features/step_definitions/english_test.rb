Given(/^ENG I am on the GL test environment website$/) do
  $reusable_utilities.open
  url_end = '/#/'
  @page_checks.urlcheck(url_end)
  @page_checks.dynamic_content_check
end

And(/^ENG I select to take the English test$/) do
  screen_name = 'Familiarisation Test'
  $reusable_utilities.screenshot_capture(screen_name)
  @familiarisation_screen.english
end

And(/^ENG I select start test button$/) do
  screen_name = 'Taking the Test'
  $reusable_utilities.screenshot_capture(screen_name)
  # $browser.button(label: /Start test/).click
  @taking_test.starttest
end

And(/^ENG I choose next button after reading the instruction$/) do
  screen_name = 'Intruction screen'
  $reusable_utilities.screenshot_capture(screen_name)
  @reusable_elements.nextbuttonselected
end

When(/^ENG I answer the first 2 set of questions and choose the next button$/) do
  screen_name = 'Twins Question'
  $reusable_utilities.screenshot_capture(screen_name)
  $browser.element(label: /three quarters/).click
  @reusable_elements.nextbuttonselected
  $browser.element(label: /They are written in the past tense/).click
  @reusable_elements.nextbuttonselected
end

And(/^ENG I answer another 2 set of questions and select the next button again$/) do
  @reusable_elements.nextbuttonselected
  screen_name = 'Second Question Instruction'
  $reusable_utilities.screenshot_capture(screen_name)
  $browser.label(:text => 'him').click
  @reusable_elements.nextbuttonselected
  $browser.label(:text => 'are').click
  @reusable_elements.nextbuttonselected
end

And(/^ENG I select the answers for the next 3 set of questions$/) do
  @reusable_elements.nextbuttonselected
  $browser.label(:text => 'No Mistake').click
  @reusable_elements.nextbuttonselected
  $browser.label(:text => 'No Mistake').click
  @reusable_elements.nextbuttonselected
  $browser.label(:text => 'No Mistake').click
  @reusable_elements.nextbuttonselected
end

And(/^ENG I select the submit button to end the test$/) do
@end_test.endtest
end

Then(/^ENG I will see "([^"]*)" displayed on the sreen$/) do |output|
url_end = '/#/'
@page_checks.urlcheck(url_end)
@page_checks.dynamic_content_check
end
