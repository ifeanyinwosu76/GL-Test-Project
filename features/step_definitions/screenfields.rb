require 'rubygems'
require 'cucumber'
require 'page-object'
require "watir"
require 'test/unit/assertions'
include Test::Unit::Assertions
# require 'minitest/autorun'
# require "pry"

class FamiliarisationTestScreen
  include PageObject
  link(:english, xpath: '/html/body/app-root/app-test-selector/div/div[2]/div/ul/li[4]/a')
end

class TakingTest
  include PageObject
  button(:starttest, class: "start-test-button")
end

class TwinQuestions
  include PageObject
  radio(:threequarter, class: "multi-choice-answer-label")
end

class EndTest
  include PageObject
  button(:endtest, class: 'end-test-button')
end
