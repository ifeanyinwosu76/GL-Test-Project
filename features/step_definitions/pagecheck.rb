require 'rubygems'
require 'cucumber'
require 'page-object'
require "watir"
require 'test/unit/assertions'
include Test::Unit::Assertions
require 'minitest/autorun'
# require "pry"

class PageChecks
include PageObject

def urlcheck(url_end)
  #assert_equal 'https://tst.services.nhsbsa.nhs.uk/scrutinise-a-death/' + url_end, @browser.url, 'URL check failed'
  url = $GL_ACCEPTANCE_TEST_URL + url_end
  assert_equal url.downcase, @browser.url.downcase, 'URL check failed'
end

def page_title_check(screen_name)
  begin
    #check for lede paragraph existing
    if $browser.title == ' '
    else
      title_exist = true
      assert_equal true, title_exist, screen_name + ' Screen: Title element does not exist or is not shown'
    end
    #rescue
    #puts 'title check failed'
    #$browser.quit
  end
end

def back_button_check
  #Back Link exists
  back_link = $browser.text.include? 'Back'
  assert_equal true, back_link, 'Back link is missing'
end

def dynamic_content_check
  subject1 = $browser.text.include? 'Maths'
  subject2 = $browser.text.include? 'Verbal'
  subject3 = $browser.text.include? 'Non-Verbal'
  subject4 = $browser.text.include? 'English'
  assert_equal true, subject1, + 'This is not on the page, this test does not exist'
  assert_equal true, subject2, + 'This is not on the page, this test does not exist'
  assert_equal true, subject3, + 'This is not on the page, this test does not exist'
  assert_equal true, subject4, + 'This is not on the page, this test does not exist'

end

def heading_check(screen_name)
  #check for header existing
  assert_equal true, $browser.element(class: 'heading-large').exist?, screen_name + ' Screen: Heading element does not exist or is not shown'
end
end
