Before do

      #$CI_SELENIUM_HUB = ''
      # $CI_SELENIUM_HUB = ''
      #
      #
      $GL_ACCEPTANCE_TEST_URL = 'https://testwise.testingforschools.com/tests/player-demo-iseb'
      $DEV_ACCEPTANCE_TEST_URL = ''
      $PIPELINE_ACCEPTANCE_TEST_URL = ''


      # $MESDatabase = PG.connect("connection url here .com", "5432", "", "", "database name here", "rebho", "1165df200fc907acd594")

      # BROWSER & CAPABILITY INITIALISATIONS
      # ************************************
      case ENV['execution']   # Execution Settings
      when "local", "development", "test", "staged", "production"         # Local Machine execution
            $DEV_ACCEPTANCE_TEST_URL
            $PIPELINE_ACCEPTANCE_TEST_URL
            $GL_ACCEPTANCE_TEST_URL


            case ENV['browser'] # Browser Initialisation
            when 'chrome'

                  #1 minute default wait for watir
                  Watir.default_timeout = 60
                  $browser = Watir::Browser.new :chrome
                  $browsertype = 'CHR'
                  $browser.window.maximize

                  # 2 minute default wait for watir
                  ##Watir.default_timeout = 1800
                  ##client = Selenium::WebDriver::Remote::Http::Default.new
                  ##client.timeout = 1800
                  ##$browser =  Watir::Browser.new :chrome, :http_client => client
                  ##$browser.window.maximize

            when 'chrome_headless'
                  Watir.default_timeout = 9000

                  $browser = Watir::Browser.new :chrome, headless: true
                  $browsertype = 'CHR'

                  $browser.window.maximize

            when 'firefox'
                  Watir.default_timeout = 9000

                  $browser = Watir::Browser.new :firefox
                  $browsertype = 'FF'

                  $browser.window.maximize

            when 'ie'
                  Watir.default_timeout = 9000

                  $browser = Watir::Browser.new :internet_explorer
                  $browsertype = 'ie'

                  #  $browser.window.maximize

            when 'safari'
                  Watir.default_timeout = 9000

                  $browser = Watir::Browser.new :safari
                  $browsertype = 'SAF'

                  $browser.driver.manage.window.maximize

            when 'opera'
                  $browser = Watir::Browser.new :opera
                  $browsertype = 'OP'

            end

      when 'bs'              # Browser Stack Initialisation
            case ENV['platform'] # Platform Initialisation
            when 'browser'
                  caps = Selenium::WebDriver::Remote::Capabilities.new

                  caps["os"] = ENV['os']
                  caps["os_version"] = ENV['os_version']
                  caps["browser"] = ENV['browser']
                  caps["browser_version"] = ENV['browser_version']
                  caps['javascriptEnabled'] = 'true'
                  caps['browserstack.local'] = 'true'
                  # Project settings
                  caps["project"] = "MES"
                  caps["build"] = ENV['build']
                  caps["name"] = ENV['name']

                  # FIXED Variables - not to be changed but checked before execution
                  # ****************************************************************
                  caps["browserstack.local"] = "true"
                  caps["browserstack.selenium_version"] = "3.5.2"
                  caps["browserstack.debug"] = "true"
                  caps["browserstack.networkLogs"] = "true"
                  caps["autoGrantPermissions"] = "true"
                  caps["autoAcceptAlerts"]= "true"
                  caps["browserstack.console"] ="errors"
                  # WINDOWS EXAMPLE
                  # caps["os"] = "Windows"
                  # caps["os_version"] = "7"
                  # caps["browser"] = "IE"
                  # caps["browser_version"] = "8.0"

                  # caps["os"] = "OS X"
                  # caps["os_version"] = "Sierra"
                  # caps["browser"] = "Safari"
                  # caps["browser_version"] = "10.1"

            when 'mobile'
                  caps = Selenium::WebDriver::Remote::Capabilities.new
                  caps["os_version"] = ENV['os_version']
                  caps["device"] = ENV['device']

                  # Project settings
                  caps["project"] = "MES"
                  caps["build"] = ENV['build']
                  caps["name"] = ENV['name']

                  # FIXED Variables - not to be changed but checked before execution
                  # ****************************************************************
                  caps["real_mobile"] = "true"
                  caps["browserstack.local"] = "true"
                  caps["browserstack.debug"] = "true"
                  caps["browserstack.video"] = "true"
                  caps["browserstack.networkLogs"] = "true"



                  # IPHONE EXAMPLE
                  # caps["os_version"] = "11.0"
                  # caps["device"] = "iPhone X"

                  # ANDROID EXAMPLE
                  # caps["os_version"] = "4.4"
                  # caps["device"] = "Samsung Galaxy Tab 4"

                  # IPAD EXAMPLE
                  # caps["os_version"] = "11.3"
                  # caps["device"] = "iPad 6th"


                  # caps['browserName'] = 'Chrome'
                  # caps['version'] = '66x64'
                  # caps['platform'] = 'Mac OSX 10.13'
                  # caps['screenResolution'] = '1366x768'

            end

            $browser = Watir::Browser.new(:remote,
                  :url => "http://tester4087:JnqXdzJn3yYt2prY9mvf@hub-cloud.browserstack.com/wd/hub",
                  # :url => "http://adeel62:zxj1NLeinAPfh8p4s3qG@hub-cloud.browserstack.com/wd/hub",
                  :desired_capabilities => caps)
                  $browser.goto $CLOUD_ACCEPTANCE_TEST_URL
                  #$browser.driver.manage.window.maximize
                  $browsertype = 'Browserstack'

            when 'pipeline'

                  case ENV['browser']
                  when 'chrome'
                        # 2 minute default wait for watir
                        Watir.default_timeout = 120

                        $browsertype = 'CHR'

                        # Selenium Grid Execution
                        $browser = Watir::Browser.new :chrome, url: $CI_SELENIUM_HUB

                  when 'firefox'
                        # 2 minute default wait for watir
                        Watir.default_timeout = 120

                        $browsertype = 'FF'

                        # Selenium Grid Execution
                        $browser = Watir::Browser.new :firefox, url: $CI_SELENIUM_HUB

                  when 'chrome_headless'
                        # 2 minute default wait for watir
                        Watir.default_timeout = 120

                        $browsertype = 'HLess'

                        $browser = Watir::Browser.new(:chrome, {:chromeOptions => {:args => ['--headless', '--no-cache', '--ignore-certificate-errors', '--disable-gpu', '--no-sandbox']}})

                        # $browser.window.resize_to(1920, 1080)
                        $browser.window.resize_to(1366, 768)
                        # $browser.window.maximize
                  end
            end

            # Initialisation of all libraries and screens to be used in the scripts
            $reusable_utilities = ReusableUtilities.new($browser)
            @reusable_elements = ReusableElements.new($browser)
            @page_checks = PageChecks.new($browser)
            @familiarisation_screen = FamiliarisationTestScreen.new($browser)
            @taking_test = TakingTest.new($browser)
            @twin_question = TwinQuestions.new($browser)
            @end_test = EndTest.new($browser)

      end
